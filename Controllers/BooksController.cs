﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VirtualLibrary.API.Dtos;
using VirtualLibrary.API.Helpers;
using VirtualLibrary.API.Interfaces;

namespace VirtualLibrary.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookRepository _repo;
        private readonly IMapper _mapper;

        public BooksController(IBookRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IActionResult> GetBooks([FromQuery]UserParams userParams)
        {
            var books = await _repo.GetBooks(userParams);

            var booksForReturn = _mapper.Map<IEnumerable<BooksToReturn>>(books);

            Response.AddPagination(books.CurrentPage, books.PageSize, books.TotalCount, books.TotalPages);

            return Ok(booksForReturn);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetBook(int id)
        {
            var book = await _repo.GetBook(id);

            if (book != null)
                return Ok(book);
            else
                return BadRequest("Book doesn't exist in repository");
        }
    }
}