﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualLibrary.API.Helpers
{
    public class UserParams
    {
        private const int maxPageSize = 30;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 18;
        public int PageSize
        {
            get { return pageSize; }
            set { pageSize = (value > maxPageSize) ? maxPageSize : value; }
        }

        public string Genue { get; set; }
        public string Author { get; set; }
    }
}
