﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualLibrary.API.Dtos;
using VirtualLibrary.API.Models;

namespace VirtualLibrary.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Book, BooksToReturn>();
            CreateMap<User, UserForReturnDto>();
            CreateMap<LoginDto, User>();
            CreateMap<RegisterDto, User>();
        }
    }
}
