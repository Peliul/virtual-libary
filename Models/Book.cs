﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualLibrary.API.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublishedAt { get; set; }
        public string Publishers { get; set; }
        public string ISNB { get; set; }
        public string Genue { get; set; }
        public string CoverUrl { get; set; }
    }
}
