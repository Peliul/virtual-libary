﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualLibrary.API.Models
{
    public class Favorite
    {
        public int BookId { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public Book Book { get; set; }
    }
}
