﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualLibrary.API.Helpers;
using VirtualLibrary.API.Models;

namespace VirtualLibrary.API.Interfaces
{
    public interface IBookRepository
    {
        void Add<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<PagedList<Book>> GetBooks(UserParams userParams);
        Task<Book> GetBook(int id);
        //Task<List<string>> GetFilters();
    }
}
