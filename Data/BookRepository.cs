﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualLibrary.API.Interfaces;
using VirtualLibrary.API.Models;
using VirtualLibrary.API.Helpers;
using Microsoft.EntityFrameworkCore;

namespace VirtualLibrary.API.Data
{
    public class BookRepository : IBookRepository
    {
        private readonly DataContext _context;

        public BookRepository(DataContext context)
        {
            _context = context;
        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public async Task<Book> GetBook(int id)
        {
            var book = await _context.Books.FirstOrDefaultAsync(i => i.Id == id);

            return book;
        }

        public async Task<PagedList<Book>> GetBooks(UserParams userParams)
        {
            var books = _context.Books.AsQueryable(); 

            if(!userParams.Genue.Equals("All"))
            {
                books = _context.Books.Where(g => g.Genue == userParams.Genue);
            }

            if(!userParams.Author.Equals("All"))
            {
                books = _context.Books.Where(g => g.Author == userParams.Author);
            }
            
            return await PagedList<Book>.CreateAsync(books, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
