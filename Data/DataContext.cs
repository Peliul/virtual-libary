﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualLibrary.API.Models;

namespace VirtualLibrary.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Favorite> Favorites { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Favorite>()
                .HasKey(k => new { k.UserId, k.BookId });

            builder.Entity<Favorite>()
                .HasOne(u => u.User)
                .WithMany(b => b.FavoriteBooks)
                .HasForeignKey(f => f.BookId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
